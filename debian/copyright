Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dtkwidget
Upstream-Contact: https://github.com/linuxdeepin/dtkwidget/issues
Source: https://github.com/linuxdeepin/dtkwidget

Files: *
Copyright: 2010-2021, Deepin Technology Co., Ltd.
           2021, Uniontech Technology Co., Ltd.
           2021, Uniontech Software Technology Co.,Ltd.
License: LGPL-3+

Files: debian/*
Copyright: 2010-2018, Deepin Technology Co., Ltd.
           2017-2020, Boyuan Yang <073plan@gmail.com>
           2020, Arun Kumar Pariyar <openarungeek@gmail.com>
           2022, Clay Stan <claystan97@gmail.com>
License: LGPL-3+

Files: tools/translate_generation.sh
Copyright: 2010-2018, Deepin Technology Co., Ltd.
           2017, shibowen <shibowen@linuxdeepin.com>
License: LGPL-3+

Files: conanfile.py
Copyright: Iceyer <me@iceyer.net>
License: GPL-2+

Files:
 docs/doxygentheme/doxygen-awesome*
Copyright:
 2021-2022 jothepro
License: Expat

Files:
       src/widgets/dsearchcombobox.*
       src/widgets/dtoolbutton.*
       src/widgets/dwindowquitfullbutton.*
       src/widgets/private/dprintpreviewdialog_p.h
       src/widgets/private/dprintpreviewwidget_p.h
       src/widgets/private/dsearchcombobox_p.h
       src/widgets/dprintpickcolorwidget.cpp
       include/dtkwidget/widgets/dprintpickcolorwidget.h
       include/dtkwidget/widgets/dprintpreviewdialog.h
       include/dtkwidget/widgets/dprintpreviewwidget.h
       plugin/dtkuidemo/*.cpp
       plugin/dtkuidemo/*.h
       plugin/dtkuiplugin/*.h
       plugin/dtkuiplugin/*.cpp
Copyright: 2019-2021, Uniontech Software Technology Co.,Ltd.
           2020, Deepin Technology Co., Ltd.
License: GPL-3+

License: LGPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
